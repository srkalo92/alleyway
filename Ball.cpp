#include "Ball.h"

Ball::Ball()
{
    radius = 8;
    start();
}

void Ball::draw(SDL_Renderer* renderer){
   if(hidden){
        return;
   }
   const int32_t diameter = (radius * 2);

   int32_t x = (radius - 1);
   int32_t y = 0;
   int32_t tx = 1;
   int32_t ty = 1;
   int32_t error = (tx - diameter);

   SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);

   while (x >= y)
   {
      //  Each of the following renders an octant of the circle
      SDL_RenderDrawPoint(renderer, centreX + x, centreY - y);
      SDL_RenderDrawPoint(renderer, centreX + x, centreY + y);
      SDL_RenderDrawPoint(renderer, centreX - x, centreY - y);
      SDL_RenderDrawPoint(renderer, centreX - x, centreY + y);
      SDL_RenderDrawPoint(renderer, centreX + y, centreY - x);
      SDL_RenderDrawPoint(renderer, centreX + y, centreY + x);
      SDL_RenderDrawPoint(renderer, centreX - y, centreY - x);
      SDL_RenderDrawPoint(renderer, centreX - y, centreY + x);

      if (error <= 0)
      {
         ++y;
         error += ty;
         ty += 2;
      }

      if (error > 0)
      {
         --x;
         tx += 2;
         error += (tx - diameter);
      }
   }
}

void Ball::move(){
   move(2,2);
}

void Ball::move(int dx, int dy){

    if(state == 0){
        return;
    }
    if(centreX + dx < 0 + radius){
        state += 1;
    }
    if(centreX + dx > 640 - radius){
        state -= 1;
    }
    if(centreY + dy < 0 + radius){
        state += 4;
    }
    if(centreY + dy > 480 - radius){
        state -= 4;
    }

    if(state & 1){
        dx = -dx;
    }
    if(state & 4){
        dy = -dy;
    }
    centreX += dx;
    centreY += dy;
}

int Ball::getX(){
    return centreX;
}

int Ball::getY(){
    return centreY;
}

int Ball::getR(){
    return radius;
}

void Ball::collide(short int change){
    state += change;
}

void Ball::stop(){
    state = 0;
    hidden = true;

}

void Ball::start(){
    centreX = 400;
    centreY = 250;
    state = LEFT_DOWN;
    hidden = false;
}

void Ball::setY(int y){
    centreY = y;
}
