#include "Platform.h"

Platform::Platform()
{
    platformRect = new SDL_Rect();
    platformRect->x = 270;
    platformRect->y = 400;
    platformRect->w = 100;
    platformRect->h = 10;
}

Platform::~Platform()
{
    delete platformRect;
}

void Platform::draw(SDL_Renderer* renderer) {
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderDrawRect(renderer, platformRect);
}

void Platform::move(int dx, int dy) {
    if(platformRect->x + dx < 0 || platformRect->x + dx > 640 - platformRect->w) return;
    platformRect->x += dx;
}

short int Platform::getState() {
    return state;
}

void Platform::setState(short int state) {
    this->state = state;
}

void Platform::move() {
    if(state != 0) {
        if(state & 1) {
            move(-3, 0);
        }
        if(state & 2) {
            move(3, 0);
        }
    }
}

int Platform::getX(){
    return platformRect->x;
}

int Platform::getY(){
    return platformRect->y;
}

int Platform::getW(){
    return platformRect->w;
}

int Platform::getH(){
    return platformRect->h;
}
