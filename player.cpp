#include "player.h"

Player::Player(Platform *platform) : Drawable(), Movable(), KeyboardEventListener() {
    this->platform = platform;
}

void Player::draw(SDL_Renderer *renderer) {
    platform->draw(renderer);
}
void Player::move() {
    platform->move();
}

void Player::move(int dx, int dy) {
    platform->move(dx, dy);
}

void Player::listen(SDL_KeyboardEvent &event) {
    if(event.type == SDL_KEYDOWN) {
        if(event.keysym.sym == SDLK_LEFT) {
            platform->setState(platform->getState()|Platform::LEFT);
        } else if(event.keysym.sym == SDLK_RIGHT) {
            platform->setState(platform->getState()|Platform::RIGHT);
        }
    } else if (event.type == SDL_KEYUP) {
        if(event.keysym.sym == SDLK_LEFT) {
            platform->setState(platform->getState()&~Platform::LEFT);
        } else if(event.keysym.sym == SDLK_RIGHT) {
            platform->setState(platform->getState()&~Platform::RIGHT);
        }
    }

}
