#include "Score.h"

Score::Score(int total): total(total), score(total)
{
    font = TTF_OpenFont("lazy.ttf", 24);
}

Score::~Score()
{
    delete font;
}

void Score::draw(SDL_Renderer* renderer){
  SDL_Color white = {255, 255, 255};
  stringstream ss;
  ss << "[" << score<<"/"<<total << "]";
  SDL_Surface* sm = TTF_RenderText_Solid(font, ss.str().c_str(), white);
  SDL_Texture* poruka = SDL_CreateTextureFromSurface(renderer, sm);
  SDL_Rect score_rect;
  score_rect.x = 280;
  score_rect.y = 450;
  score_rect.w = sm->w;
  score_rect.h = sm->h;
  SDL_RenderCopy(renderer, poruka, NULL, &score_rect);

}

void Score::updateScore(){
    score--;
}
