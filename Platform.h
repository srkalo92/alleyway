#ifndef PLATFORM_H
#define PLATFORM_H

#include <stdio.h>
#include <iostream>

#include "drawable.h"
#include "movable.h"
#include "keyboardeventlistener.h"

using namespace std;

class Platform : public Drawable, public Movable
{
    public:
        enum State:short int {STOP=0, LEFT=1, RIGHT=2};

        Platform();
        virtual ~Platform();

        void draw(SDL_Renderer* renderer);
        void move();
        void move(int dx, int dy);
        short int getState();
        void setState(short int state);
        int getX();
        int getY();
        int getW();
        int getH();
    private:
        SDL_Rect *platformRect;
        short int state;
};

#endif // PLATFORM_H
