#ifndef BALL_H
#define BALL_H

#include "drawable.h"
#include "movable.h"

class Ball: public Drawable, public Movable
{
    public:
        enum State:short int {STOP=0, LEFT=1, RIGHT=2, UP=4, DOWN=8,
                     LEFT_UP=LEFT|UP, LEFT_DOWN=LEFT|DOWN,
                     RIGHT_UP=RIGHT|UP, RIGHT_DOWN=RIGHT|DOWN};

        Ball();
        void draw(SDL_Renderer* renderer);
        void move();
        void move(int dx, int dy);
        int getX();
        int getY();
        int getR();
        void setY(int y);
        void collide(short int);
        void stop();
        void start();
    private:
        short int state;
        int centreX;
        int centreY;
        int radius;
        bool hidden;
};

#endif // BALL_H
