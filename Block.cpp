#include "Block.h"

Block::Block(int x, int y)
{
    platformRect = new SDL_Rect();
    platformRect->w = 30;
    platformRect->h = 15;
    platformRect->x = x;
    platformRect->y = y;
    hidden = false;
}

Block::~Block()
{
    delete platformRect;
}

void Block::draw(SDL_Renderer* renderer) {
    if(hidden) return;
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderDrawRect(renderer, platformRect);
}

int Block::getX(){
    return platformRect->x;
}

int Block::getY(){
    return platformRect->y;
}

int Block::getW(){
    return platformRect->w;
}

int Block::getH(){
    return platformRect->h;
}

void Block::hide(){
    hidden = true;
}

bool Block::isHidden(){
    return hidden;
}

