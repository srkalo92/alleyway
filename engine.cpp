#include "engine.h"

ostream& operator<<(ostream& out, const Level& l) {
    int rows = l.getLevelMatrix().size();
    int cols = 0;
    if(rows > 0) {
        cols = l.getLevelMatrix()[0].size();
    }
    out << rows << " " << cols << endl;

    for(int i = 0; i < rows; i++){
        for(int j = 0; j < cols; j++) {
            out << l.getLevelMatrix()[i][j] << " ";
        }
        out << endl;
    }

    return out;
}

Engine::Engine(string title) {
    SDL_Init(SDL_INIT_VIDEO);
    IMG_Init(IMG_INIT_PNG);
    window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_UNDEFINED,
                              SDL_WINDOWPOS_UNDEFINED, 640, 480, SDL_WINDOW_RESIZABLE);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    TTF_Init();
}

void Engine::addTileset(Tileset *tileset, const string &name) {
    tilesets[name] = tileset;
}

void Engine::addTileset(istream &inputStream, const string &name) {
    addTileset(new Tileset(inputStream, renderer), name);
}

void Engine::addTileset(const string &path, const string &name) {
    ifstream tilesetStream(path.c_str());
    addTileset(tilesetStream, name);
}

Tileset* Engine::getTileset(const string &name) {
    return tilesets[name];
}

void Engine::addDrawable(Drawable *drawable) {
    drawables.push_back(drawable);
}

bool collisionDetect(Ball* ball, Block* block){
    int distX = abs(ball->getX() - block->getX() - block->getW()/2);
    int distY = abs(ball->getY() - block->getY() - block->getH()/2);

    if (distX > (block->getW()/2 + ball->getR())) { return false; }
    if (distY > (block->getH()/2 + ball->getR())) { return false; }

    if (distX <= (block->getW()/2)) { return true; }
    if (distY <= (block->getH()/2)) { return true; }

    // also test for corner collisions
    int dx=distX-block->getW()/2;
    int dy=distY-block->getH()/2;
    return dx * dx + dy * dy <= ball->getR() * ball->getR();
}

bool collisionDetect(Ball* ball, Platform* block){
    int distX = abs(ball->getX() - block->getX() - block->getW()/2);
    int distY = abs(ball->getY() - block->getY() - block->getH()/2);

    if (distX > (block->getW()/2 + ball->getR())) { return false; }
    if (distY > (block->getH()/2 + ball->getR())) { return false; }

    if (distX <= (block->getW()/2)) { return true; }
    if (distY <= (block->getH()/2)) { return true; }

    // also test for corner collisions
    int dx=distX-block->getW()/2;
    int dy=distY-block->getH()/2;
    return dx * dx + dy * dy <= ball->getR() * ball->getR();
}

void Engine::run() {
    int maxDelay = 1000/frameCap;
    int frameStart = 0;
    int frameEnd = 0;

    bool running = true;
    SDL_Event event;


    Platform *platform = new Platform();

    Player *player = new Player(platform);
    drawables.push_back(player);
    movables.push_back(player);
    eventListeners.push_back(player);

    Ball* ball = new Ball();
    drawables.push_back(ball);
    movables.push_back(ball);

    list<Block*> blocks;
    for(int i = 0; i < 5; i++){
        for(int j = 0; j < 18; j++){
            Block* block = new Block(50 + (30*j), 30 + (15*i));
            blocks.push_back(block);
            drawables.push_back(block);
        }
    }

    Score* score = new Score(blocks.size());
    drawables.push_back(score);

    while(running) {
        frameStart = SDL_GetTicks();
        while(SDL_PollEvent(&event)) {
            if(event.type == SDL_QUIT) {
                running = false;
            } else {
                for(size_t i = 0; i < eventListeners.size(); i++) {
                    eventListeners[i]->listen(event);
                }
            }

            // Obrisi me kasnije
            if(event.type == SDL_KEYDOWN) {
                SDL_KeyboardEvent e = event.key;
                if(e.keysym.sym == SDLK_SPACE) {
                    ball->start();
                }
            }

        }

        for(size_t i = 0; i < movables.size(); i++) {
            movables[i]->move();
        }



        if(collisionDetect(ball, platform)){
            ball->setY(platform->getY() - ball->getR());
            ball->collide(-4);
        }

        if( ball->getY() + ball->getR() >= 479){
                ball->stop();
        }

        list<Block*>::iterator it = blocks.begin();
        for(; it != blocks.end(); it++){
            if((*it)->isHidden()) continue;
            if(collisionDetect(ball, *it)){
                ball->collide(4);
                (*it)->hide();
                score->updateScore();
            }
        }

        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
        SDL_RenderClear(renderer);

        for(size_t i = 0; i < drawables.size(); i++) {
            drawables[i]->draw(renderer);
        }

        SDL_RenderPresent(renderer);

        frameEnd = SDL_GetTicks();
        if(frameEnd - frameStart < maxDelay) {
            SDL_Delay(maxDelay - (frameEnd - frameStart));
        }
    }
}

Engine::~Engine() {
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    TTF_Quit();
    SDL_Quit();
}
