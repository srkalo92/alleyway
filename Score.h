#ifndef SCORE_H
#define SCORE_H

#include "drawable.h"
#include <SDL2/SDL_ttf.h>
#include <iostream>
#include <sstream>
using namespace std;

class Score: public Drawable
{
    public:
        Score(int total);
        virtual ~Score();

        void draw(SDL_Renderer* renderer);
        void updateScore();
    private:
        int score;
        int total;
        TTF_Font* font;
};

#endif // SCORE_H
