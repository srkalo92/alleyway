#ifndef BLOCK_H
#define BLOCK_H

#include "drawable.h"

class Block : public Drawable
{
    public:
        Block(int x, int y);
        virtual ~Block();
        void draw(SDL_Renderer* renderer);
        int getX();
        int getY();
        int getW();
        int getH();
        bool isHidden();
        void hide();

    private:
        SDL_Rect *platformRect;
        bool hidden;
};

#endif // BLOCK_H
